//Caroline Carlton
//CSE 002 lab03
//9-13-18
/*Program calculates how much each person in a party 
should pay when splitting the check evenly; includes tip */
///
import java.util.Scanner;
public class Check {
  // main method required for every Java program
  public static void main (String [] args){
    Scanner myScanner = new Scanner( System.in );

    //Asks user to put in the original cost of their check
System.out.print("Enter the original cost of the check in the form xx.xx: ");
  double checkCost = myScanner.nextDouble(); //Accepts user input

    //Asks user to put in the amount of tip they wish to give
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
  double tipPercent = myScanner.nextDouble(); //Accepts user input
  tipPercent /= 100; //Converts the percentage into a decimal value

    //Asks user to enter the number of people trying to split the check
System.out.print("Enter the number of people who went out to dinner: ");
  int numPeople = myScanner.nextInt(); //Accepts user input

  double totalCost;     //Declares the total cost
  double costPerPerson; // Declares the cost per person
  int dollars,          //Declares the whole dollar amount of cost 
    dimes, pennies;     //for storing digits
                        //to the right of the decimal point 
                        //for the cost$ 
totalCost = checkCost * (1 + tipPercent); //Calculates total cost
costPerPerson = totalCost / numPeople;    //Calcualtes cost per person

dollars=(int)costPerPerson;             //get the whole amount, dropping decimal fraction
dimes=(int)(costPerPerson * 10) % 10;   //get dimes amount
pennies=(int)(costPerPerson * 100) % 10;//get pennies amount
    
    //Prints the amount each person owes
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
   
  } //end of main method
} //end of class