//Caroline Carlton
//8-31-18
//CSE HW01 WelcomeClass
///

public class WelcomeClass{
  
  public static void main(String args[]){
    
    //Prints Welcome to terminal window
      System.out.println("    -----------");
      System.out.println("    | WELCOME |");
      System.out.println("    -----------");
    
    //Prints CEC221 to terminal window
      System.out.println("  ^  ^  ^  ^  ^  ^");
      System.out.println(" / \\ / \\/ \\/ \\/ \\/ \\");
      System.out.println("<-C--E--C--2--2--1->");
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      System.out.println("  v  v  v  v  v  v");
    
    //Prints Autobiographic statement to terminal window
      System.out.print("My name is Caroline and I am a sophomore at Lehigh University");
                         
    
  }
}