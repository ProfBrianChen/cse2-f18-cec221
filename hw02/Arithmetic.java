// Caroline Carlton
// CSE 002 hw02
// 9-6-18
/// Computes the cost of items bought, including the PA sales tax of 6%

public class Arithmetic {
  public static void main (String [] args){
 
//Declares and assigns values to the number of or cost of various items
int numPants = 3;   //Number of pairs of pants
double pantsPrice = 34.98;   //Cost per pair of pants
    
int numShirts = 2;   //Number of sweatshirts
double shirtPrice = 24.99;   //Cost per shirt

int numBelts = 1;  //Number of belts
double beltPrice = 33.99;   //Cost per belt

double paSalesTax = 0.06;  //the tax rate
    
//Declares total costs of each individual item
double totalCostOfPants;   //total cost of pants
double totalCostOfShirts;  //total cost of sweatshirts
double totalCostOfBelts;   //total cost of belts
    
  //Assigns values to the total costs of each individual item
  totalCostOfPants = numPants * pantsPrice;     //total cost of pants
  totalCostOfShirts = numShirts * shirtPrice;   //total cost of shirts
  totalCostOfBelts = numBelts * beltPrice;      //total cost of belts
    
    //Prints the total cost of pants
    System.out.println("Total cost of pants = $" + totalCostOfPants);
    //Prints the total cost of sweatshirts
    System.out.println("Total cost of sweatshirts = $" +totalCostOfShirts);
    //Prints the total cost of belts
    System.out.println("Total cost of belts = $" + totalCostOfBelts);
    
    //Prints blank line
    System.out.println();
    
    //Prints the sales tax for pants
    System.out.println("Sales tax of pants = $" + 
                       String.format("%.2f",(paSalesTax*totalCostOfPants)));
    //Prints the sales tax for sweatshirts
    System.out.println("Sales tax of sweatshirts = $" + 
                       String.format("%.2f",(paSalesTax*totalCostOfShirts)));
    //Prints the sales tax for belts
    System.out.println("Sales tax of belts = $" + 
                       String.format("%.2f", (paSalesTax*totalCostOfBelts)));
    
    //Prints blank line
    System.out.println();
             
//Declares and calculates value for the total cost of the items before sales tax
double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;  
//Declares and calculates value for the Sales tax on all items total
double totalSalestax = totalCost * paSalesTax;
/*Declares and calculates value for the total amount paid for
  in the transaction, including sales tax */
double totalTransaction = totalCost + totalSalestax;
    
    //Prints the total cost of items before sales tax
    System.out.println("Total cost before sales tax = $" + String.format("%.2f", totalCost));
    //Prints the total Sales tax
    System.out.println("Total sales tax = $" + String.format("%.2f", totalSalestax));
    //Prints the total amount paid for in the transaction with sales tax
    System.out.println("Total amount paid for transaction with sales tax = $" +
                       String.format("%.2f", totalTransaction));

    
  }
}