//Caroline Carlton
//CSE 002 hw05
//10-8-18
/*The following program calculates the probabilty of getting four 
different types of hands (One Pair, Two Pair, Three-Of-A-Kind, Four-Of-A-Kind) 
after however many hands the user inputs to try*/
///
import java.util.Scanner;
import java.lang.Math;
public class Hw05 {
  public static void main (String [] args){
    Scanner scnr = new Scanner (System.in);
//Asks user for the amount of hands they want to go through for probability
    System.out.println("State amount of hands you wish to calculate");
    int userInput = scnr.nextInt();
    int counter = 1;  //Counts the number of times the program calculates a hand 
    int onePair = 0;  //Amount of One-Pair hands there are
    int twoPair = 0;  //Amount of Two-Pair hands there are
    int threeOfAKind = 0;  //Amount of hands with Three-of-a-kind
    int fourOfAKind = 0;   //Amount of hands with Four-of-a-kind

    while (counter < (userInput +1)){ //runs the program however many times the user requests   
//Declares and assigns random values 1-52 to each card in a single hand    
      int card1 = (int)(Math.random()*52) + 1;
      int card2 = (int)(Math.random()*52) + 1;
      int card3 = (int)(Math.random()*52) + 1;
      int card4 = (int)(Math.random()*52) + 1;
      int card5 = (int)(Math.random()*52) + 1;

//The following while statements make sure that no cards are repeated
      while (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5){
        card1 = (int)(Math.random()*52) + 1;
      }
      while (card2 == card3 || card2 == card4 || card2 == card5){
        card2 = (int)(Math.random()*52) + 1;
      }
      while (card3 == card4 || card3 ==card5){
        card3 = (int)(Math.random()*52) + 1;
      }
      while (card4 == card5){
        card4 = (int)(Math.random()*52) + 1;
      }

//The following modules cut the random numbers 1-52 into 13 possible options:
 // (Ace, 1, 2...Queen,King)
      card1 %= 13;
      card2 %= 13; 
      card3 %= 13; 
      card4 %= 13; 
      card5 %= 13; 
   
      int match = 0; //Counter for how many matches there are of single pairs
//If there are four cards with the same identity, increment int fourOfAKind by one      
      if (card1 == card2 && card1 == card3 && card1 == card4 ||
          card1 == card2 && card1 == card3 && card1 == card5 ||
          card1 == card2 && card1 == card4 && card1 == card5 ||
          card1 == card3 && card1 == card4 && card1 == card5 ||
          card2 == card3 && card2 == card4 && card2 == card5){
        fourOfAKind++;
      }
 //If there are three cards with the same identity, increment int threeOfAKind by one
      else if (card1 == card2 && card1 == card3 ||
               card1 == card2 && card1 == card4 ||
               card1 == card2 && card1 == card5 ||
               card1 == card3 && card1 == card4 ||
               card1 == card3 && card1 == card5 ||
               card1 == card4 && card1 == card5 ||
               card2 == card3 && card2 == card4 ||
               card2 == card4 && card2 == card5 ||
               card2 == card3 && card2 == card5 ||
               card3 == card4 && card3 == card5) {
        threeOfAKind++;
      }
 //This else statement is used to calculate the One-Pairs and Two-Pairs
 //Each time there is a pair, int match is incremented by one
      else {
        if (card1==card2){
          match++;
        }
        if (card1==card3){
          match++;
        }
        if (card1==card4){
          match++;
        }
        if (card1==card5){
          match++;
        }
         if (card2==card3){
          match++;
        }
        if (card2==card4){
          match++;
        }
        if (card2==card5){
          match++;
        }
        if (card3==card4){
          match++;
        }
        if (card3==card5){
          match++;
        }
        if (card4==card5){
          match++;
        }
      }
 //If there is only one match, then onePair is incremented by 1
      if (match == 1){
        onePair++;
      }
 //If there are two matches, then twoPair is incremented by 1
      else if (match == 2){
        twoPair++;
      }
 //If there are zero matches, there is no hand
      else{
      }
         
      counter++; //This increments every time the program runs 
                 // until the user's input is reached
    }
    
 //The following statements calculate the probability of getting each hand after all trials
    double probabilityHand1 = (double) onePair/userInput;
    double probabilityHand2 = (double) twoPair/userInput;
    double probabilityHand3 = (double) threeOfAKind/userInput;
    double probabilityHand4 = (double) fourOfAKind/userInput;
 //The following prints out the number of loops the user requested     
      System.out.println("The number of loops: " + userInput);
 //The following prints the probabilities calculated above to the third decimal place
      System.out.printf("The probability of One-pair: %.3f \n", probabilityHand1);
      System.out.printf("The probability of Two-pair: %.3f \n", probabilityHand2);
      System.out.printf("The probability of Three-of-a-kind: %.3f \n", probabilityHand3);
      System.out.printf("The probability of Four-of-a-kind: %.3f \n", probabilityHand4);
     
  }
}