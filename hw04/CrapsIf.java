//Caroline Carlton
//CSE 02 hw04
//9-25-18
/* Helps user practice the game of Craps! 
   User can ask for a random cast of their dice 
   or they can put in the values of their own. 
   After each roll, the program outputs a statement 
   that tells the user what the correct slang term
   that is used to describe their roll is. 
   --- this program only uses if/else statements. */
///
import java.util.Random;
import java.util.Scanner;
public class CrapsIf{
  public static void main(String[] args){
    Random randGen = new Random();
    Scanner scnr = new Scanner (System.in);
    /*Asks user if they would like to have their dice randomly cast;
      accepts only "yes" and "no" */
    System.out.println("Would you like to randomly cast dice?");
    String userInput = scnr.next(); //Reads the user's input
      //Goes through calculations if the user wants to be randomly cast dice
      if (userInput.equals("yes")){
        int dieNum1 = randGen.nextInt(6) + 1; //Picks a random die number (1-6)
        int dieNum2 = randGen.nextInt(6) + 1; //Picks another random die number (1-6)
        //Prints out statement telling the user what numbers were chosen
        System.out.println("You rolled a " + dieNum1 + " and a " + dieNum2);  
          if(dieNum1 == 1 && dieNum2 == 1){       //If user rolls two ones,
            System.out.println("Snake Eyes!");    // prints "Snake Eyes!"
          }                                      
          else if (dieNum1 == 2 && dieNum2 == 2){ //If user rolls two twos,
            System.out.println("Hard Four!");     // prints "Hard Four!"
          }
          else if (dieNum1 == 3 && dieNum2 == 3){ //If user rolls two threes,
            System.out.println("Hard Six!");      // prints "Hard Six!"
          }
          else if (dieNum1 == 4 && dieNum2 == 4){ //If user rolls two fours,
            System.out.println("Hard Eight!");    // prints "Hard Eight"
          }
          else if (dieNum1 == 5 && dieNum2 == 5){ //If user rolls two fives,
            System.out.println("Hard Ten!");      // prints "Hard Ten"
          }
          else if (dieNum1 == 6 && dieNum2 == 6){ //If user rolls two sixes,
            System.out.println("Boxcars!");       // prints "Boxcars!"
          }
          else if (dieNum1+dieNum2 == 3){         //If the sum of the dice equals 3
            System.out.println("Ace Deuce!");     // prints "Ace Deuce!"
          }
          else if (dieNum1+dieNum2 == 4){         //If the sum of the dice equals 4
            System.out.println("Easy Four!");     // prints "Easy Four!"
          }
          else if (dieNum1+dieNum2 == 5){         //If the sum of the dice equals 5
            System.out.println("Fever Five!");    // prints "Fever Five!"
          }
          else if (dieNum1+dieNum2 == 6){         //If the sum of the dice equals 6
            System.out.println("Easy Six!");      // prints "Easy Six!"
          }
          else if (dieNum1+dieNum2 == 7){         //If the sum of the dice equals 7
            System.out.println("Seven out!");     // prints "Seven out!"
          }
          else if (dieNum1+dieNum2 == 8){         //If the sum of the dice equals 8
            System.out.println("Easy Eight!");    // prints "Easy Eight!"
          }
          else if (dieNum1+dieNum2 == 9){         //If the sum of the dice equals 9
            System.out.println("Nine!");          // prints "Nine!"
          }
          else if (dieNum1+dieNum2 == 10){        //If the sum of the dice equals 10
            System.out.println("Easy Ten!");      // prints "Easy Ten!"
          }
          else if (dieNum1+dieNum2 == 11){        //If the sum of the dice equals 11
            System.out.println("Yo-leven!");      // prints "Yo-leven!"
          }
    }     
    else if (userInput.equals("no")){
      System.out.println("State the value on the first die");
      int dieNum1 = scnr.nextInt();
      System.out.println("State the value on the second die");
      int dieNum2 = scnr.nextInt();
      //Makes sure the integers the user inputs are within the range 1-6
        if(dieNum1 <= 6 && dieNum1 >= 1 && dieNum2 <= 6 && dieNum2 >= 1){
          if(dieNum1 == 1 && dieNum2 == 1){       //If user rolls two ones,
            System.out.println("Snake Eyes!");    // prints "Snake Eyes!"
          }                                      
          else if (dieNum1 == 2 && dieNum2 == 2){ //If user rolls two twos,
            System.out.println("Hard Four!");     // prints "Hard Four!"
          }
          else if (dieNum1 == 3 && dieNum2 == 3){ //If user rolls two threes,
            System.out.println("Hard Six!");      // prints "Hard Six!"
          }
          else if (dieNum1 == 4 && dieNum2 == 4){ //If user rolls two fours,
            System.out.println("Hard Eight!");    // prints "Hard Eight"
          }
          else if (dieNum1 == 5 && dieNum2 == 5){ //If user rolls two fives,
            System.out.println("Hard Ten!");      // prints "Hard Ten"
          }
          else if (dieNum1 == 6 && dieNum2 == 6){ //If user rolls two sixes,
            System.out.println("Boxcars!");       // prints "Boxcars!"
          }
          else if (dieNum1+dieNum2 == 3){         //If the sum of the dice equals 3
            System.out.println("Ace Deuce!");     // prints "Ace Deuce!"
          }
          else if (dieNum1+dieNum2 == 4){         //If the sum of the dice equals 4
            System.out.println("Easy Four!");     // prints "Easy Four!"
          }
          else if (dieNum1+dieNum2 == 5){         //If the sum of the dice equals 5
            System.out.println("Fever Five!");    // prints "Fever Five!"
          }
          else if (dieNum1+dieNum2 == 6){         //If the sum of the dice equals 6
            System.out.println("Easy Six!");      // prints "Easy Six!"
          }
          else if (dieNum1+dieNum2 == 7){         //If the sum of the dice equals 7
            System.out.println("Seven out!");     // prints "Seven out!"
          }
          else if (dieNum1+dieNum2 == 8){         //If the sum of the dice equals 8
            System.out.println("Easy Eight!");    // prints "Easy Eight!"
          }
          else if (dieNum1+dieNum2 == 9){         //If the sum of the dice equals 9
            System.out.println("Nine!");          // prints "Nine!"
          }
          else if (dieNum1+dieNum2 == 10){        //If the sum of the dice equals 10
            System.out.println("Easy Ten!");      // prints "Easy Ten!"
          }
          else if (dieNum1+dieNum2 == 11){        //If the sum of the dice equals 11
            System.out.println("Yo-leven!");      // prints "Yo-leven!"
          }
        }
        else {
          System.out.println("Invalid response"); //Prints "Invalid response" if user input is wrong
        }
    }
    else {
      System.out.println("Invalid response");     //Prints "Invalid response" if user input is wrong
    }  
  }
}