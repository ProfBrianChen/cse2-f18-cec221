//Caroline Carlton
//CSE 02 hw04 
//9-25-18
/* Helps user practice the game of Craps! 
   User can ask for a random cast of their dice 
   or they can put in the values of their own. 
   After each roll, the program outputs a statement 
   that tells the user what the correct slang term
   that is used to describe their roll is. 
   --- this program only uses switch statements. */
///
import java.util.Random;
import java.util.Scanner;
public class CrapsSwitch{
  public static void main (String [] args){
    Random randGen = new Random();
    Scanner scnr = new Scanner(System.in);
    /* Asks the user whether they would like to have
    dice randomly cast to them or if they would like to choose their own values.
    The numbers 1 and 2 are used to record their response. */
    System.out.println("Would you like to randomly cast dice (Type 1)");
    System.out.println("or would you like to state the two dice you want to evaluate (Type 2)?");
    int userInput = scnr.nextInt();
    switch(userInput)
    {
      case 1: // This case is used if the user wants dice randomly cast
        int dieNum1 = randGen.nextInt(6) + 1; //Picks a random die number (1-6)
        int dieNum2 = randGen.nextInt(6) + 1; //Picks another random die number (1-6)
        //Prints out statement telling the user what numbers were chosen
        System.out.println("You rolled a " + dieNum1 + " and a " + dieNum2);
        //Assigns isEqual with true if both dice are the same number
        boolean isEqual = dieNum1 == dieNum2;
        // randomCastInt is assigned 1 if isEqual is true and 0 if it is false
        int randomCastInt = isEqual ? 1 : 0; 
        int randomCastSum = (dieNum1 + dieNum2); //Assigns and declares the sum of the two dice values
        switch (randomCastInt)
        {
          case 1: // if both the dice are the same number the following program runs
            switch (randomCastSum)
            {
              case 2:                              //If the sum equals 2 (both dice are 1)
                System.out.println("Snake Eyes!"); // prints statement "Snake Eyes!"
                break;
              case 4:                              //If the sum equals 4 (both dice are 2)
                System.out.println("Hard Four!");  // prints statement "Hard Four!"
                break;
              case 6:                              //If the sum equals 6 (both dice are 3)
                System.out.println("Hard Six!");   // prints statement "Hard Six!"
                break;
              case 8:                              //If the sum equals 8 (both dice are 4)
                System.out.println("Hard Eight!"); // prints statement "Hard Eight!"
                break;
              case 10:                             //If the sum equals 10 (both dice are 5)
                System.out.println("Hard Ten!");   // prints statement "Hard Ten!"
                break;
              case 12:                             //If the sum equals 12 (both dice are 6)
                System.out.println("Boxcars!");    // prints statement "Boxcars!"
                break;
            }
            break;
          case 0: // if the dice are two different numbers the following program runs
            switch (randomCastSum)
            {
              case 3:                               //If the sum of the dice equals 3
                System.out.println("Ace Deuce!");   // prints statement "Ace Deuce!"
                break;
              case 4:                               //If the sum of the dice equals 4
                System.out.println("Easy Four!");   // prints statement "Easy Four!"
                break;
              case 5:                               //If the sum of the dice equals 5
                System.out.println("Fever Five!");  // prints statement "Fever Five!"
                break;
              case 6:                               //If the sum of the dice equals 6
                System.out.println("Easy Six!");    // prints statement "Easy Six!"
                break;
              case 7:                               //If the sum of the dice equals 7
                System.out.println("Seven out!");   // prints statement "Seven out!"
                break;
              case 8:                               //If the sum of the dice equals 8
                System.out.println("Easy Eight!");  // prints statement "Easy Eight!"
                break;
              case 9:                               //If the sum of the dice equals 9
                System.out.println("Nine!");        // prints statement "Nine!"
                break;
              case 10:                              //If the sum of the dice equals 10
                System.out.println("Easy Ten!");    // prints statement "Easy Ten!"
                break;
              case 11:                              //If the sum of the dice equals 11
                System.out.println("Yo-leven!");    // prints statement "Yo-leven!"
                break;
            }
            break;
        }
        break;
      case 2: // This case is used if the user wants to input their own values
        //Asks for user to input the dice values they wish to use
        System.out.println("State the value on the first die");
        int choiceDieNum1 = scnr.nextInt();
        System.out.println("State the value on the second die");
        int choiceDieNum2 = scnr.nextInt();
        //Assigs numRangeBool with true if the user input is within the range 1-6
        boolean numRangeBool = choiceDieNum1 <= 6 && choiceDieNum1 >= 1
                               && choiceDieNum2 <= 6 && choiceDieNum2 >=1;
        //Assigns numRangeInt with 1 when the above condition is true and 0 when it is false
        int numRangeInt = numRangeBool ? 1 : 0; 
        switch (numRangeInt)
        {
          case 1: //This case is used when the numbers the user picks fall within the range 1-6
            //Assigns and declares the sum of the two dice values
            int choiceSum = (choiceDieNum1 + choiceDieNum2); 
            //Assigns isEqual with true if both dice are the same number
            isEqual = choiceDieNum1 == choiceDieNum2;
            //choiceInt is assigned 1 if isEqual is true and 0 if it is false
            int choiceInt = isEqual ? 1 : 0;
            switch (choiceInt)
            {
              case 1: // if both the dice are the same number the following program runs
                switch (choiceSum)
                {
                    case 2:                              //If the sum equals 2 (both dice are 1)
                      System.out.println("Snake Eyes!"); // prints statement "Snake Eyes!"
                      break;
                    case 4:                              //If the sum equals 4 (both dice are 2)
                      System.out.println("Hard Four!");  // prints statement "Hard Four!"
                      break;
                    case 6:                              //If the sum equals 6 (both dice are 3)
                      System.out.println("Hard Six!");   // prints statement "Hard Six!"
                      break;
                   case 8:                              //If the sum equals 8 (both dice are 4)
                      System.out.println("Hard Eight!"); // prints statement "Hard Eight!"
                      break;
                    case 10:                             //If the sum equals 10 (both dice are 5)
                      System.out.println("Hard Ten!");   // prints statement "Hard Ten!"
                      break;
                    case 12:                             //If the sum equals 12 (both dice are 6)
                      System.out.println("Boxcars!");    // prints statement "Boxcars!"
                      break;
                }
                break;
              case 0:  // if the dice are two different numbers the following program runs
                switch (choiceSum)
                {
                    case 3:                               //If the sum of the dice equals 3
                      System.out.println("Ace Deuce!");   // prints statement "Ace Deuce!"
                      break;
                    case 4:                               //If the sum of the dice equals 4
                      System.out.println("Easy Four!");   // prints statement "Easy Four!"
                      break;
                    case 5:                               //If the sum of the dice equals 5
                      System.out.println("Fever Five!");  // prints statement "Fever Five!"
                      break;
                    case 6:                               //If the sum of the dice equals 6
                      System.out.println("Easy Six!");    // prints statement "Easy Six!"
                      break;
                    case 7:                               //If the sum of the dice equals 7
                      System.out.println("Seven out!");   // prints statement "Seven out!"
                      break;
                    case 8:                               //If the sum of the dice equals 8
                      System.out.println("Easy Eight!");  // prints statement "Easy Eight!"
                      break;
                    case 9:                               //If the sum of the dice equals 9
                      System.out.println("Nine!");        // prints statement "Nine!"
                      break;
                    case 10:                              //If the sum of the dice equals 10
                      System.out.println("Easy Ten!");    // prints statement "Easy Ten!"
                      break;
                    case 11:                              //If the sum of the dice equals 11
                      System.out.println("Yo-leven!");    // prints statement "Yo-leven!"
                      break;
                }
                break;         
            }
            break;
          case 0: // Prints "Invalid answer" if the user types a number out of the range 1-6
              System.out.println("Invalid answer");
              break;
        }    
        break;
      default: // Prints "Invalid answer" if the user does not provide the correct input
        System.out.println("Invalid answer");
        break; 
    }
  }
}
