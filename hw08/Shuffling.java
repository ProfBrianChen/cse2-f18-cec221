//Caroline Carlton
//CSE 002 hw08
//11-13-18
//This program creates an array of cards, shuffles that array, and picks out a hand for the user
///
import java.util.Scanner;
public class Shuffling{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; // Declares array cards
    String[] hand = new String[5];   // Declares array hand
    
    int numCards = 5; // Declares int numCards = 5
    int again = 1;    // Declares int again = 1
    int index = 51;   // Declares int index = 51
    
    // puts the rank and suit names into the cards array
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13];  
      System.out.print(cards[i] + " ");
    } 
    
    System.out.println();
    printArray(cards); // Calls the printArray method
    
    //Calls the shuffled method
    System.out.println("Shuffled:");
    shuffle(cards);
    printArray(cards);
    
    //Calls the getHand method when again==1
    System.out.println();
    System.out.println("Hand:");
    while(again == 1){ 
      hand = getHand(cards,index,numCards); 
      printArray(hand); // calls printArray with the new hand
      index -= numCards; 
      
      //Used if the number of cards runs out
      if (index - numCards <= 0) {
        shuffle(cards);
        index = 51;
      }
      
      //Draws another hand
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
  
  // This method prints the array that is put in it
  public static String[] printArray(String[] list){
    for (int i = 0; i<list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
      return list;
  }
  
  // This method shuffles a string array
  public static String[] shuffle(String[] list){   
 
    for (int i=0; i<list.length; i++) {
      
      //Generates a random number
      int randomPosition = (int)Math.random()*(list.length-1);
      
      //Swaps values
      String temp = list[i];
      list[i] = list[randomPosition];
      list[randomPosition] = temp;
    }
 
    return list;
 }
  
  //This method chooses a random hand of cards
  public static String[] getHand(String[] list, int index, int numCards){
    String [] stringArray = new String[numCards];
      for (int i = 0; i < numCards; i++){
        stringArray[i] = list[index-i];
        index--;
      }
    return stringArray;
  }
}
