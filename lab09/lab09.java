//Caroline Carlton
//CSE 002 lab09
// Illustrates the effect of passing arrays as method arguments by inverting arrays.
///
public class lab09{
  public static void main (String[] args){
    int [] array0 = {1, 2, 3, 4, 5, 6, 7, 8}; //Declares array0 and assigns with 8 ints
    int [] array1 = copy(array0); //Declares array1 and assigns it with a copy of array0
    int [] array2 = copy(array0); //Declares array2 and assigns it with a copy of array0
 
    //Passes array0 to inverter() and prints it out with print()
    inverter(array0); 
    print(array0);
    System.out.println();

    //Passes array1 to inverter2() and prints it out with print()
    inverter2(array1);
    print(array1);
    System.out.println();
    
    //assigns array3 with array2 and prints it out with print()
    int [] array3 = new int [array2.length];
    array3 = inverter2(array2);     //Passes array2 to inverter2()
    print(array3);
    
  }
  
  //makes a copy of the integer array that is passed through the method
  public static int[] copy (int[] array){
    int [] newArray = new int [array.length];
    for(int i = 0; i < array.length; i++){
      newArray[i] = array[i];
    }
    return newArray;
  }

  //reverses the order of an array
  public static void inverter (int[] array){
    for (int i = 0; i<array.length/2; i++){
      int temp = array[i];
      array[i] = array[array.length - i - 1];
      array[array.length - i - 1] = temp;
    }
  }
  
  //uses copy() to make a copy of the input array then uses a copy of the
  // code from inverter() to invert the members of the copy
  public static int[] inverter2 (int[] array){
    int [] copy = new int [array.length];
    copy = copy(array);
    inverter(copy);
    return copy;
  }

  //prints arrays
  public static void print (int[] array){
    for (int i = 0; i < array.length; i++){
      System.out.print(array[i]);
    }
  }

}