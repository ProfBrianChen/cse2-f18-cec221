//Caroline Carlton
//CSE 002 lab08
// Creates an array that with 100 randomized numbers and calls a method to count the number of occurences
///
import java.util.Random;
public class lab08{
  public static void main (String [] args){
    
    Random rand = new Random(); // Random Generator
    
    int[] myArray1 = new int[100]; // Initializes myArray1 with a length of 100

    for (int i = 0; i < myArray1.length; ++i){
      
      myArray1[i]= rand.nextInt(100); // Assigns a random number from 1-100 to each index
      
      countOccurences(myArray1, 100, myArray1[i]); // Calls method myArray2()
    }   
  }
  public static void countOccurences(int myArray1[], int n, int x) { 
    
        int counter = 0; 
        
        for (int i=0; i<n; i++) {
          
          if (x == myArray1[i]) {
            
              counter++; // Counts the amount of times that myArray1[i] occurs 
              
          }
        }
        
        // Prints the amount of occurances of each number in myArray1
        System.out.println(x + " occurs " + counter + " times"); 
  }
}
    