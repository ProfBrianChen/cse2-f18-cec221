//Caroline Carlton
//CSE 002 lab04
//9-20-28
//
///
import java.lang.Math;
public class CardGenerator2{
  public static void main(String[]args){
    int cardNum = (int)(Math.random()*12)+2;
    int faceType = (int)(Math.random()*4);
    
  if ((cardNum >= 2) && (cardNum <=10)){
    System.out.print("You picked the " + cardNum + " of ");
  }
  else {
      switch (cardNum){  
      case 11: 
        System.out.print("You picked the King of ");//The number 11 will translate to be a King
        break;
      case 12:
        System.out.print("You picked the Ace of ");//The number 12 will translate to be an Ace
        break;
      case 13: 
        System.out.print("You picked the Jack of ");//The number 13 will translate to be a Jack
        break;
      case 14:
        System.out.print("You picked the Queen of ");//The number 14 will translate to be a Queen
        break;
       }      
  }
   switch (faceType){
      case 0:
        System.out.println("Hearts");//The number 0 will print as Hearts
        break;
      case 1:
        System.out.println("Diamonds");//The number 1 will print as Diamonds
        break;
      case 2: 
        System.out.println("Clubs");//The number 2 will print as Clubs
        break;
      case 3:
        System.out.println("Spades");//The number 3 will print as Spades
       break;
   }
  }
}