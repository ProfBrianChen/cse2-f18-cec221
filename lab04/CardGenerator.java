//Caroline Carlton
//CSE 002 lab04
//9-20-18
// Generates random card numbers and suits
///
import java.util.Random;
public class CardGenerator{
  public static void main (String [] args){
    Random randGen = new Random (); 
    int cardNum = randGen.nextInt(13); //Generates random integer from 0-12
    int faceType = randGen.nextInt(4); //Generates random integer from 0-3
    
    switch (cardNum){  
      case 0: 
        System.out.print("You picked the King of ");//The number 0 will translate to be a King
        break;
      case 1:
        System.out.print("You picked the Ace of ");//The number 1 will translate to be an Ace
        break;
      case 11: 
        System.out.print("You picked the Jack of ");//The number 11 will translate to be a Jack
        break;
      case 12:
        System.out.print("You picked the Queen of ");//The number 12 will translate to be a Queen
        break;
      default:
        System.out.print("You picked the " + cardNum + " of ");//Other card numbers will print as itself
      }
    switch (faceType){
      case 0:
        System.out.println("Hearts");//The number 0 will print as Hearts
        break;
      case 1:
        System.out.println("Diamonds");//The number 1 will print as Diamonds
        break;
      case 2: 
        System.out.println("Clubs");//The number 2 will print as Clubs
        break;
      case 3:
        System.out.println("Spades");//The number 3 will print as Spades
    }
    
  }
}