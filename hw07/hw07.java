//Caroline Carlton
//CSE hw07
//The following code makes several edits to a user's string
/// 
import java.util.Scanner;
public class hw07{
public static void main (String [] args){
  Scanner scnr = new Scanner (System.in); // Calls method sampleText to main method, declares it as a String
  String str = sampleText(); // Calls method printMenu
  printMenu(); // Needed in order to scan String by each character
  char input = scnr.next().charAt(0); // Declares user input char as input
  // If user types 'c', calls method getNumOfNonWSCharacters()
  // If the user inputs an incorrect character
  while (input != 'c' && input != 'w' && input != 'f' && input != 'r' && input != 's' && input != 'q') {
    System.out.println("You entered an incorrect character, try again"); // Prints error statement
    input = scnr.next().charAt(0);
  }
  if (input == 'c'){
    System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(str));  
  }
  // If user types 'w', calls method getNumOfWords()
  else if (input == 'w'){
    System.out.println("Number of words: " + getNumOfWords(str));
  }
  // If user types 'f', asks for which word to find and calls method findText()
  else if (input == 'f'){
    System.out.println("Enter a word or phrase to be found:");
    String userWord = scnr.next();
    System.out.println("\"" + userWord + "\" instances: " + findText(str, userWord));
  }
  // If user types 'r', calls method replaceExclamation()
  else if (input == 'r'){
    System.out.println("Edited text: " + replaceExclamation(str));
  }
  // If user types 'r', calls method shortenSpace()
  else if (input == 's'){
    System.out.println("Edited text: " + shortenSpace(str));
  }
  // If user types 'q', quits program
  else if (input == 'q'){
    System.exit(0);
  }
}
    // Prompts user for sample text and returns the string they inputted back to main method
  public static String sampleText(){
  Scanner scnr = new Scanner (System.in);
  System.out.println("Enter a sample text: ");
  String userString = scnr.nextLine();
  System.out.println("You entered: " + userString);
  return userString;
  }
    // Prints the menu of options the user can choose from
  public static void printMenu(){
   System.out.println("MENU");
   System.out.println("c - Number of non-whitespace characters");
   System.out.println("w - Number of words");
   System.out.println("f - Find text");
   System.out.println("r - Replace all !'s");
   System.out.println("s - Shorten spaces");
   System.out.println("q - Quit");
   System.out.println();
   System.out.println("Choose an option:");
  }
    // Finds the number of non white space characters and returns number back to main method
  public static int getNumOfNonWSCharacters(String str){
    int counter = 0;
    for (int i = 0; i < str.length(); i++){ // Parses through the string
      if(str.charAt(i) != ' '){  // If the char is not a space, add to counter
        counter++;
      }
    }
    return counter;
  }
    // Finds the number of words and returns number back to main method
  public static int getNumOfWords(String str){
    int counter = 1;
    for (int i = 0; i < str.length(); i++){ // Parses through the string
      if (str.charAt(i) == ' ' && str.charAt(i) == str.charAt(i+1)){
        str = str.replace("  ", " "); // If there is a double white space, returns it to single
      }
    }
      // Assumed that for every word there is a space separating it from the next one.
      // Checked above to make sure that there are not accidental double spaces  
    for (int i = 0; i < str.length(); i++){ // Parses through the string
      if (str.charAt(i) == ' '){  // If the char is a space, add to counter
        counter++;
      }
    }
    return counter;
  }
    // Finds the number of instances the user inputted word appears in the user string
  public static int findText(String str, String userWord){
    int counter = 0;
    int wordLength = userWord.length();
   // Parses through the user string in chunks as long as the user word
    // -(wordLength-1) ensures the program does not parse past the size of the user string
    for(int i = 0; i < str.length()-(wordLength-1); i++){ 
      // Tries to see if the substring userWord is within the main string the user inputted
      if (str.substring(i,i + wordLength).equals(userWord)){
        counter++;
      }
    }
    return counter;
  }
    // Finds any !'s and replaces with .'s, returns edited string to main method
  public static String replaceExclamation(String str){
    for (int i = 0; i < str.length(); i++){
      if (str.charAt(i) == '!'){
        str = str.replace("!",".");
      }
    }
    return str;
  }
    // Finds any double spaces and correct them to one space, returns edited string to main method
  public static String shortenSpace(String str){
    for (int i = 0; i < str.length(); i++){
      if (str.charAt(i) == ' ' && str.charAt(i) == str.charAt(i+1)){
        str = str.replace("  ", " ");

      }
    }
    return str;
  }
}
      

