//Caroline Carlton
//CSE 002 lab06
//10-11-18
//The following program makes Pattern C
///
import java.util.Scanner;
public class PatternC {
  public static void main (String [] args) {
    Scanner scnr = new Scanner (System.in);
    //Asks user for an integer between 1 and 10
    System.out.println("Type an integer between 1 and 10");
    int input = scnr.nextInt(); //Assigns user's input to "input"
    int i; //Declares int i
    int j; //Declares int j
    int space; //Declares int space
    //Makes sure the user inputs a number between 1 and 10
    while (input > 10 || input < 1){
      //If user enters incorrect number, error message is displayed
      System.out.println("Error value out of range, try again");
      input = scnr.nextInt();
      }
    //Creates a loop that lists all the numbers counting up to the input
    // in ascending order
    for (i=1; i<=input; ++i){
      //Creates a loop that will make spaces equivalent to the user input
      //Spaces will decrease after each line
      for(space = 1; space <= input - i; ++space) {
        System.out.print("  ");
      }
      //Creates a loop to decrement each number before being printed
      // again on the same line
      for (j=i;j>=1; --j){
        System.out.print(j + " ");
      }
        System.out.println(); //Separates the lines
    } 
  }
}