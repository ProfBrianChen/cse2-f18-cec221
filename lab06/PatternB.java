//Caroline Carlton
//CSE 002 lab06
//10-11-18
//The following program makes Pattern B
///
import java.util.Scanner;
public class PatternB {
  public static void main (String [] args) {
    Scanner scnr = new Scanner (System.in);
    //Asks user for an integer between 1 and 10
    System.out.println("Type an integer between 1 and 10");
    int input = scnr.nextInt(); //Assigns user's input to "input"
    int i; //Declares int i
    int j; //Declares int j
    //Makes sure the user inputs a number between 1 and 10
    while (input > 10 || input < 1){
      //If user enters incorrect number, error message is displayed
      System.out.println("Error value out of range, try again");
      input = scnr.nextInt();
    }
    //Creates a loop that starts by printing all the numbers leading 
    // up to input and decrements after each run
    for (i=input; i>=1; --i){
      //Creates a loop to increment each number before being printed again
      for (j=1;j<=i; ++j){
        System.out.print(j + " ");
      }
      System.out.println(); //Separates the lines
    } 
  }
}