// Caroline Carlton
// CSE 002 Lab02
//9-6-18
/* Calculates the amount of minutes on the cyclometer per trip;
  States the number of rotation counts of the wheel per trip;
  calculates the individual and total distances of the two trips taken in miles; */

public class Cyclometer {
  public static void main(String [] args) {
    
    // Declares and assigns values to various variables
  int secsTrip1=480;  // states the amount of seconds in trip 1
  int secsTrip2=3220;  // states the amount of seconds in trip 2
	int countsTrip1=1561;  // states the amount of rotations in trip 1
	int countsTrip2=9037; // states the amount of roatations in trip 2
    
    // Declares and assigns values to various consants
    double wheelDiameter=27.0,  // states the diameter of the bike wheel
  	PI=3.14159, // Pi constant
  	feetPerMile=5280,  // states the amount of feet in a mile
  	inchesPerFoot=12,   // states the amount of inches in a foot
  	secondsPerMinute=60;  // states the number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  // declares variables sought in output
  
    // Prints number of minutes and counts for Trip 1
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
    //Prints number of minutes and counts for Trip 2
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    
    // Declares and assigns values to various variables
    distanceTrip1=countsTrip1*wheelDiameter*PI; // gives distance in inches for Trip 1
    distanceTrip1/=inchesPerFoot*feetPerMile; // gives distance in miles for Trip 1
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // gives distance in miles for Trip 2
	  totalDistance=distanceTrip1+distanceTrip2; // calculates the total distance of both trips combined
  
    // Prints out the distance of Trip 1  
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
   // Prints out the distance of Trip 2 
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
    // Prints out the distance of both trips combined
	System.out.println("The total distance was "+totalDistance+" miles");



  

    
  }
}