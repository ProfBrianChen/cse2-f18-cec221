//Caroline Carlton
//CSE 002 hw09
/* this method creates random arrays of size 10 and then proceeds to
 * utilize different methods to either delete the a specific position 
 * in the array or delete specific values seen in the array. */
///
import java.util.*;
public class RemoveElements{
  public static void main(String [] arg){
    
   //Code given by Professor 
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
 
      System.out.print("Enter the index ");
      index = scan.nextInt();
      // Prints an error message if user's input is out of bounds
      while (index > 9 || index <0){
        System.out.println("Error: out of bounds");
      }
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
   
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y")); 
  }

  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  
  /* This method generates an array of 10 random integers between 0 to 9 */
  public static int [] randomInput(){
    Random rand = new Random(); // Random Generator
    
    int[] arr = new int[10]; // Initializes arr with a length of 10

    for (int i = 0; i < arr.length; ++i){    
      arr[i]= rand.nextInt(10); // Assigns a random number from 1-10 to each index   
    }    
    return arr; // Returns filled randomized array
  }
  
  
  /* This method create a new array that has one member fewer than list, and be 
   * composed of all of the same members except the member in the position pos */  
  public static int [] delete(int [] list, int pos){
    int [] newArray = new int[9]; // Initializes newArray with a length of 9
    
    for (int i = 0; i < list.length; i++){
      if (i == pos){ // After target position is found,
      break;         // breaks out of for loop
      }
      else{
        newArray[i] = list[i]; // copys list[] onto newArray[]
      }
    }
    for (int i = pos; i < newArray.length; i++){
      newArray[i] = list[i + 1]; // must skip ahead in list[] in order to delete pos
    }
    return newArray; // returns array with deleted index
  }
        
  
  /* This method deletes all the elements that are equal to 
   * target, returning a new list without all those new elements */
  public static int [] remove(int [] list,int target){ 
    int i;
    int j;
    int counter = 0; // Initializes a counter and sets it to 0
    for (i = 0; i < list.length; i++){
      if (list[i] == target){ // when the target value is found
        counter++;            // increment counter by one
      }
    }
    if(counter == 0){ // is the number they requested is not in the list,
      return list;    // return the original list
    }
    else{
      // Initializes newArray that is shortened by the number of times the target is found
      int [] newArray = new int [list.length - counter];
      for (i = 0, j = 0; i < newArray.length; i++, j++){
        while (list[j] == target){ // everytime the index = target
          j++;                     // increment j by one
        }
      newArray[i] = list[j]; // copy the list with new indexes onto newArray
    }
    return newArray; // return a new array with deleted values
    }
  }
}
