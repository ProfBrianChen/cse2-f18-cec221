//Caroline Carlton
//CSE 002 hw09
/* This code uses binary search and linear search to sort through an array 
 * provided by the user and find whether or not a key value was included. 
 * It also keeps track of the number of iterations the code goes through 
 * in order to find the key */
///
import java.util.*;
public class CSE2Linear {
  public static void main (String [] args){
    Scanner scan = new Scanner(System.in);
    // Prompts user for final grades
    System.out.println("enter 15 ints for your final grades in CSE2. Write them from least to greatest");
    
    // Makes sure that the user inputs an int
    while (!scan.hasNextInt()){
      System.out.println("Error: Incorrect Type");
      scan.next();
    }
    
    int userInput = scan.nextInt(); // assigns the user's input to userInput
    
    // Makes sure that the user's input is between 0-100
    while (userInput > 100 || userInput < 0){
      System.out.println("Error: Out of Range");
      userInput = scan.nextInt();
    }
    
    int temp = 0;
    int [] array = new int [15]; // creates as array of length 15
    
    for (int i = 0; i<array.length; i++){   
      array[i] = userInput; // copies the user input into new array
      userInput = scan.nextInt();
      
      // Makes sure inpurt is less than the previous number the user inputted
      while (userInput < temp){
        System.out.println("Error: Number less than previous input");
        userInput = scan.nextInt();
      }
      temp = userInput; // uses variable temp to store the userinput for reference on next iteration
    }
    
    System.out.println(Arrays.toString(array)); // Prints Array
    
    System.out.println("Enter a grade to be searched for"); // Prompts user for key
   
    // calls method and prints the number of iterations used
    // uses code provided by Professor
    int key = scan.nextInt();
    int indKey = binarySearch2(array, key);
    System.out.println("Binary search went through " + indKey + " iterations");
    
    // calls method shuffleArray
    shuffleArray(array);
    System.out.println(Arrays.toString(array)); // prints shuffled array
    
    /* Asks user for a grade to be searched for another time in order to utilize
     * the linear search method. Proceeds to print the number of iterations used */
    System.out.println("Enter a grade to be searched for again");
    key = scan.nextInt();
    indKey = linearSearch(array, key);
    System.out.println("linear Search went through " + indKey + " iterations");
    
  }

  /* Searches through array to find the key in a binary search
   * Uses the code provided by Professor */ 
  public static int binarySearch2(int[] list, int key) {
    int low = 0;
    int high = list.length-1;
    int counter = 1;
    int mid = -1;
   
    while(high >= low) {
      mid = (low + high)/2;
      if (key < list[mid]) {
        high = mid - 1;
      }
      else if (key == list[mid]) {
        System.out.println("This grade does exist"); // print statement for when key is found
        break;
      }
      else {
        low = mid + 1;
      }
      counter++; // counts the number of iterations
    }
    if (key != list[mid]){
      System.out.println("Grade does not exist"); // print statement for when key is not found
    }
    return counter; // returns the number of iterations
  }
  
  // Shuffles the indexes in the array
  public static int[] shuffleArray(int[] array){
    Random rand = new Random();
    for (int i = array.length - 1; i > 0; i--){
      int index = rand.nextInt(i + 1);
      int temp = array[index];
      array[index] = array[i]; // swaps
      array[i] = temp;         // swaps
    }
    return array; // returns a shuffled array
  }
  
  
  /* Searches through array to find the key in a binary search */
  public static int linearSearch(int[] list, int key) { 
    int counter = 1; // initializes a counter and sets it equal to one
    int j = -1;
    for (int i = 0; i < list.length; i++) {
      if (key == list[i]) {
        System.out.println("This grade does exist");  // print statement for when key is found
        j = i;
        break;
      }
      counter++; // counts the number of iterations
    }
    if (key != list[j]){
      System.out.println("Grade does not exist"); // print statement for when key is not found
    }
    return counter; // returns the number of iterations
  }
}
    
    
    