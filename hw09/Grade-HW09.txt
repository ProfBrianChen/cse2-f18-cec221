Grading Sheet for HW9

Grade: 85

Two programs each program worth 50 pts
38
CSE2Linear.java 
Compiles    				5/5 pts
Comments 				5/5 pts
Error Checking			8/10 pts(didn't check for equal number, take 16 inputs)
Check that the user only enters ints, and print an error message if the user enters anything other than an int. 
Print a different error message for an int that is out of the range from 0-100, and finally a third error message if the int is not greater than or equal to the last int.  
Linear search method			5/10 pts(not working correctly)
Binary Search method		5/10 pts(not working correctly)
Shuffle method			10/10 pts
Take 5 points off if methods were written but did not work properly or did not follow the description of the method,

	
RemoveElements.java
47
Compiles    				5/5 pts
Comments 				5/5 pts
Randominput method		10/10 pts
The randomInput() method generates an array of 10 random integers between 0 to 9.  Implement randomInput so that it fills the array with random integers and returns the filled array. 
delete method			10/10 pts
The method delete(list,pos) takes, as input, an integer array called list and an integer called pos.  It should create a new array that has one member fewer than list, and be composed of all of the same members except the member in the position pos.  
Remove method 			10/10 pts
The method remove(list, number) takes, as input, an integer array called list and an integer called number. It should create a new array that is the length of the old array minus the number of time the number appears in the array, and be composed of all the same members except those that match the target number. I 
Works with provided main method  	10/10  pts
-3 error input does not work well
*See RemoveElements in grade folder to make sure they used provide main method


