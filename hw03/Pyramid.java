//Caroline Carlton
//CSE 02 hw03
//9-16-18
// Finds the Volume of a square pyramid
///

import java.util.Scanner;
public class Pyramid{
  public static void main(String [] args){
    Scanner scnr = new Scanner(System.in);
    
    //Asks user for the length of one of the sides of the base 
    System.out.println("The square side of the pyramid is (input length): ");
    double length = scnr.nextDouble();
    //Asks user for the height of the pyramid
    System.out.println("The height of the pyramid is (input height): ");
    double height = scnr.nextDouble();
    //Calculates and prints to the user the volume of the pyramid
    double volume = (height * Math.pow(length, 2.0)) /3;
    System.out.println("The volume inside the pyramid is: " + volume);
    
  }
}