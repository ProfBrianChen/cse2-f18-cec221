//Caroline Carlton
//CSE 02 hw03
//9-16-18
/*Calculates the amount of rainfall in cubic miles 
  given the number of inches of rain and number of acres*/
///
import java.util.Scanner; 
public class Convert{
  public static void main(String [] args){
  Scanner scnr = new Scanner(System.in);
  
  //Asks user for area in acres affected by rainfall
  System.out.println("Enter the affected area in acres: ");
  double acresAffected = scnr.nextDouble();
  //Asks user for the amount of rain in inches
  System.out.println("Enter the rainfall in the affected area: ");
  double numInches = scnr.nextDouble();
  //Calculates and prints the amount of rain in cubic miles
  double numGallons = acresAffected * numInches * 27154;
  double cubicMiles = numGallons * 9.08168597e-13;
     System.out.println(cubicMiles + " cubic miles");
    
  }
}