//Caroline Carlton
//CSE lab07
/* The following program generates a random sentence and then asks the user if they
 *  would like to have a paragraph printed out, compiling of different types of 
 *  sentences in order to create a story (The sentences should not make complete 
 *  sense, but use verbs/nouns/adjectives correctly for the most part). */
///
import java.util.Random;
import java.util.Scanner;
public class lab07{
  public static void main (String [] args){
    Scanner scnr = new Scanner (System.in); // Creates a scanner
    Random randGen = new Random();  // Creates a random generator
    
    // Declares subNoun and calls a string from method subNoun in order to keep a consistent noun
    String subNoun = subNoun(randGen);
    
    // Prints a randgom sentence
    System.out.print("The " + adj(randGen) + " " + adj(randGen) + " " + subNoun + " ");
    System.out.println(verb(randGen) + " " + "the " + adj(randGen) + " " + objNoun(randGen) + ".");
    
    // Asks user if they would like more sentences
    System.out.println("Would you like to print out a story?");
    String userInput = scnr.next();
    
    while(userInput.equals("yes")){  // If they respond "yes" the following program runs
    
      // Prints sentence 2
      System.out.print("This " + subNoun + " seemed " + adj(randGen) + " to the ");
      System.out.println(adj(randGen) + " " + objNoun(randGen) + ".");
      
      // Makes an if statement with two options in order to alternated sentence types
      if (randGen.nextInt(2) == 0){
        
        // Uses "It" rather that the subNoun for sentence 3
        System.out.print("It used the " + objNoun(randGen) + " to " + verb(randGen));
        System.out.println(" " + objNoun(randGen) + " at the " + adj(randGen) + " " + objNoun(randGen));
        
      }
      
      else{
        
        // Uses the subNoun for sentence 3
        System.out.print("The " + subNoun + " used " + objNoun(randGen) + " to " + verb(randGen));
        System.out.println(" " + objNoun(randGen) + " at the " + adj(randGen) + " " + objNoun(randGen));
        
      }
      
      // Prints a final sentence
      System.out.println("That " + subNoun + " " + verb(randGen) + " her " + objNoun(randGen) + "!");
    
      userInput = scnr.next();
    }
    
      System.exit(0); // Exits program if the user does not input "yes"
  }
  public static String adj(Random randGen) {
    int randomInt = randGen.nextInt(10); // Generates a random number 0-9
    String adjective = " ";  // Declares String adjective 
    switch(randomInt){
      case 0:
        adjective = "scratchy"; // Assigns adjective with "scratchy"
        break;
        
      case 1:
        adjective = "green"; // Assigns adjective with "green"
        break;
        
      case 2:
        adjective = "playful"; // Assigns adjective with "playful"
        break;

      case 3:
        adjective = "fuzzy"; // Assigns adjective with "fuzzy"
        break;

      case 4: 
        adjective = "crazy"; // Assigns adjective with "crazy"
        break;

      case 5:
        adjective = "lovely"; // Assigns adjective with "lovely"
        break;

      case 6:
        adjective = "wooden"; // Assigns adjective with "wooden"
        break;

      case 7:
        adjective = "bubbly"; // Assigns adjective with "bubbly"
        break;

      case 8:
        adjective = "black"; // Assigns adjective with "black"
        break;

      case 9: 
        adjective = "bright"; // Assigns adjective with "bright"
        break;
        
      default: // default case
        break;
    }
    return adjective; // returns the string assigned with adjective
  }
  public static String subNoun(Random randGen) {
    int randomInt = randGen.nextInt(10); // Generates a random number 0-9
    String subjectNoun = " ";  // Declares String subjectNoun
    switch(randomInt){
      case 0:
        subjectNoun = "squirrel"; // Assigns subjectNoun with "squirrel"
        break;
        
      case 1:
        subjectNoun = "bunny"; // Assigns subjectNoun with "bunny"
        break;

      case 2:
        subjectNoun = "tree"; // Assigns subjectNoun with "tree"
        break;

      case 3:
        subjectNoun = "olive"; // Assigns subjectNoun with "olive"
        break;

      case 4: 
        subjectNoun = "cat"; // Assigns subjectNoun with "cat"
        break;

      case 5:
        subjectNoun = "man"; // Assigns subjectNoun with "man"
        break;

      case 6:
        subjectNoun = "house"; // Assigns subjectNoun with "house"
        break;

      case 7:
        subjectNoun = "puppy"; // Assigns subjectNoun with "puppy"
        break;

      case 8:
        subjectNoun = "computer"; // Assigns subjectNoun with "computer"
        break;

      case 9: 
        subjectNoun = "flag"; // Assigns subjectNoun with "flag"
        break;

      default: // default case
        break;
    }
    return subjectNoun; // returns the string assigned with subjectNoun
  }
  public static String verb(Random randGen) {
    int randomInt = randGen.nextInt(10); // Generates a random number 0-9
    String verb = " ";  // Declares String verb
    switch(randomInt){
      case 0:
        verb = "pounces"; // Assigns verb with "pounces"
        break;
        
      case 1:
        verb = "runs towards"; // Assigns verb with "runs towards"
        break;

      case 2:
        verb = "jumps"; // Assigns verb with "jumps"
        break;

      case 3:
        verb = "laughs"; // Assigns verb with "laughs"
        break;

      case 4: 
        verb = "scare"; // Assigns verb with "scare"
        break;

      case 5:
        verb = "hug"; // Assigns verb with "hug"
        break;

      case 6:
        verb = "rubs"; // Assigns verb with "rubs"
        break;
      case 7:
        verb = "kicks"; // Assigns verb with "kicks"
        break;

      case 8:
        verb = "sings"; // Assigns verb with "sings"
        break;

      case 9: 
        verb = "pokes"; // Assigns verb with "pokes"
        break;

      default: // default case
        break;
    }
    return verb; // returns the string assigned with verb
  }
  public static String objNoun(Random randGen) {
    int randomInt = randGen.nextInt(10); // Generates a random number 0-9
    String objectNoun = " ";  // Declares String objectNoun
    switch(randomInt){
      case 0:
        objectNoun = "table"; // Assigns obejctNoun with "table"
        break;
        
      case 1:
        objectNoun = "fence"; // Assigns obejctNoun with "fence"
        break;

      case 2:
        objectNoun = "park"; // Assigns obejctNoun with "park"
        break;

      case 3:
        objectNoun = "box"; // Assigns obejctNoun with "box"
        break;

      case 4: 
        objectNoun = "stomach"; // Assigns obejctNoun with "stomach"
        break;

      case 5:
        objectNoun = "scrunchie"; // Assigns obejctNoun with "scrunchie"
        break;

      case 6:
        objectNoun = "hair"; // Assigns obejctNoun with "hair"
        break;

      case 7:
        objectNoun = "girl"; // Assigns obejctNoun with "girl"
        break;

      case 8:
        objectNoun = "boy"; // Assigns obejctNoun with "boy"
        break;

      case 9: 
        objectNoun = "snake"; // Assigns obejctNoun with "snake"
        break;

      default: // default case
        break;
    }
    return objectNoun; // returns the string assigned with objectNoun
  }
}