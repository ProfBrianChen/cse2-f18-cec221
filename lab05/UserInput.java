//Caroline Carlton
//CSE 002 lab05
//10-4-18
/* The following program asks the user for various
information regarding their class and prints back
to them the information they stated */
///
import java.util.Scanner;
public class UserInput {
  public static void main (String [] args) {
    Scanner scnr = new Scanner (System.in);
    //Declares various values found
    int courseNum;
    String deptName;
    int meetsPerWeek;
    int time;
    String profName;
    int numStudents;
    
    //Asks user for course number
    System.out.println("Type your course number");
      //Makes sure user enters an integer
      while (!scnr.hasNextInt()){
        //Prints Error: Incorrect type, try again if user does not type an integer
        System.out.println("Error: Incorrect type, try again"); 
        scnr.next();
      }
    courseNum = scnr.nextInt(); //Assigns courseNum with user input
    
    //Asks user for Department Name
    System.out.println("Type your Department Name");
    scnr.next();
      //Makes sure user enters a String
      while (!scnr.hasNextLine()){
        //Prints Error: Incorrect type, try again if user does not type a String
        System.out.println("Error: Incorrect type, try again");
        scnr.next();
      }
    deptName = scnr.nextLine(); //Assigns deptName with user input
    
    //Asks user for number of times met per week
    System.out.println("Type the number of times per week you meet");
      //Makes sure user enters an integer
      while (!scnr.hasNextInt()){
        //Prints Error: Incorrect type, try again if user does not type an integer
        System.out.println("Error: Incorrect type, try again");
        scnr.next();
      }
    meetsPerWeek = scnr.nextInt(); //Assigns meetsPerWeek with user input 
    
    //Asks user for time of class
    System.out.println("Type the time you meet");
      //Makes sure user enters an integer
      while (!scnr.hasNextInt()){
        //Prints Error: Incorrect type, try again if user does not type an integer
        System.out.println("Error: Incorrect type, try again");
        scnr.next();
      }
    time = scnr.nextInt(); //Assigns time with user input
    
    //Asks user for teacher's name
    System.out.println("Type your teacher's name");
    scnr.nextLine();
      //Makes sure user enters a String
      while (!scnr.hasNextLine()){
        //Prints Error: Incorrect type, try again if user does not type a String
        System.out.println("Error: Incorrect type, try again");
        scnr.next();
      }
    profName = scnr.nextLine(); //Assigns profName with user input
    
    //Asks user for number of students in the class
    System.out.println("Type the number of students in the class");
      //Makes sure user enters an integer
      while (!scnr.hasNextInt()){
        //Prints Error: Incorrect type, try again if user does not type an integer
        System.out.println("Error: Incorrect type, try again");
        scnr.next();
      }
    numStudents = scnr.nextInt(); //Assigns numStudents with user input
    
    //Prints out the user inputted information
    System.out.println("Course Number: " + courseNum);
    System.out.println("Department Name: " + deptName);
    System.out.println("Times met per week: " + meetsPerWeek);
    System.out.println("Time course starts: " + time);
    System.out.println("Instructor name: " + profName);
    System.out.println("Number of Students in the class: " + numStudents);
    
  }
}