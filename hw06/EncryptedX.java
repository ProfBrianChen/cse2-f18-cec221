//Caroline Carlton
//CSE hw06
//The following program prints an encrypted "X" pattern made of spaces and stars
///
import java.util.Scanner;
public class EncryptedX{
  public static void main (String [] args){
    Scanner scnr = new Scanner (System.in);
    //Asks user to input the size of the message they want
    System.out.println("Input an integer between 0 - 100"); 
    int input = scnr.nextInt();
    while (input < 0 || input > 100){
      //If the user input is out of range, prints error message
      System.out.println("Error: Out of range. Try again.");
      //Gets new input
      input = scnr.nextInt();
    }
    //Creates a loop to increment until the desired amount of rows
    for (int row = 0; row < input; row++){
      //Creates a loop to increment until the desired amount of columns
      for (int column = 0; column < input; column++){
        //Algorithm to determine where the spaces/x goes
        if (row == column || row + column == input - 1 ){
          System.out.print(" ");
        }
        //Print * in every other place 
        else {
          System.out.print("*");
        }
      }
      //Creates a new line
      System.out.println();
    }
  }
}